
Pod::Spec.new do |s|

  s.name         = "Bolts"
  s.version      = "1.2.0"
  s.summary      = "Bolts framework."

  s.description  = <<-DESC
                   Bolts framework.

                   DESC

  s.homepage     = "https://parse.com/apps/quickstart#parse_data/mobile/ios/native/new"

s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:project0-1/bolts.git" }


  s.source_files  = "*.*", "Headers/*.{h,m,swift}", "Modules/*.modulemap"



end
